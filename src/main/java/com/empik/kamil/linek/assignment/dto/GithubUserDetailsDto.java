package com.empik.kamil.linek.assignment.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class GithubUserDetailsDto extends GithubUserDto {
    private Long followers;
    @JsonProperty("public_repos")
    private Integer publicRepos;
}
