package com.empik.kamil.linek.assignment.services;

import com.empik.kamil.linek.assignment.dto.GithubUserCalculationsDto;
import com.empik.kamil.linek.assignment.dto.GithubUserDetailsDto;
import com.empik.kamil.linek.assignment.entities.LoginRequestCounter;
import com.empik.kamil.linek.assignment.repositories.LoginRequestCounterRepository;
import com.empik.kamil.linek.assignment.utils.Utils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.math.BigDecimal;

@Service
@Transactional
@Slf4j
public class GithubUserDataServiceImpl implements  GithubUserDataService{

    private final GithubApiService githubApiService;

    private final LoginRequestCounterRepository counterRepository;

    @Autowired
    public GithubUserDataServiceImpl(GithubApiService githubApiService, LoginRequestCounterRepository counterRepository) {
        this.githubApiService = githubApiService;
        this.counterRepository = counterRepository;
    }

    @Override
    public GithubUserCalculationsDto getGithubUserDtoByLogin(String login) {
        GithubUserCalculationsDto userCalculations= new GithubUserCalculationsDto();

        GithubUserDetailsDto userDetails = githubApiService.getGithubUserDetailsDtoByLogin(login);

        final BigDecimal calculation = Utils.calculate(userDetails.getFollowers(), userDetails.getPublicRepos());
        log.info("Calculations performed, result {}", calculation);

        userCalculations.setCalculations(calculation);

        BeanUtils.copyProperties(userDetails, userCalculations);

        incrementRequestCounter(login);

        return userCalculations;
    }

    private void incrementRequestCounter(String login) {
        if (counterRepository.existsByLogin(login)){
            counterRepository.incrementCounterForLogin(login);
            log.info("LoginRequestCounter incremented for {}", login);
        }else {
            LoginRequestCounter requestCounter = new LoginRequestCounter();
            requestCounter.setLogin(login);
            requestCounter.setRequestCount(1L);
            final LoginRequestCounter saved = counterRepository.save(requestCounter);
            log.info("LoginRequestCounter saved: {}", saved);
        }
    }
}
