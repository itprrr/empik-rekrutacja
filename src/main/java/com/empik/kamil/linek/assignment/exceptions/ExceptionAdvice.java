package com.empik.kamil.linek.assignment.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.reactive.function.client.WebClientResponseException;

import java.time.LocalDateTime;

@RestControllerAdvice
public class ExceptionAdvice {

    @ExceptionHandler(value = IllegalArgumentException.class)
    public ResponseEntity<ExceptionResponse> handleGenericException(IllegalArgumentException e) {
        ExceptionResponse error = new ExceptionResponse(
                HttpStatus.BAD_REQUEST.getReasonPhrase(), e.getMessage(),
                HttpStatus.BAD_REQUEST.value(), LocalDateTime.now());
        return new ResponseEntity<>(error, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(value = WebClientResponseException.class)
    public ResponseEntity<ExceptionResponse> handleGenericException(WebClientResponseException e) {
        ExceptionResponse error = new ExceptionResponse(
                HttpStatus.NOT_FOUND.getReasonPhrase(), e.getMessage(),
                HttpStatus.NOT_FOUND.value(), LocalDateTime.now());
        return new ResponseEntity<>(error, HttpStatus.NOT_FOUND);
    }

}
