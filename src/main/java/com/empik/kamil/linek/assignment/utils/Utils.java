package com.empik.kamil.linek.assignment.utils;

import java.math.BigDecimal;
import java.math.MathContext;


public final class Utils {

    private Utils() {
        throw new UnsupportedOperationException("Utils cannot be instantiated");
    }

    public static BigDecimal calculate(Long followers, Integer publicRepos){

        if(followers == null || followers == 0L){
            throw new IllegalArgumentException("Followers value cannot be zero or null");
        }

        BigDecimal result = BigDecimal.valueOf(6);

        result = result.divide(BigDecimal.valueOf(followers), MathContext.DECIMAL64).multiply(BigDecimal.valueOf(2 + publicRepos));

        return result;
    }
}
