package com.empik.kamil.linek.assignment.services;

import com.empik.kamil.linek.assignment.configuration.GithubApiConfig;
import com.empik.kamil.linek.assignment.dto.GithubUserDetailsDto;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;

import javax.transaction.Transactional;

@Service
@Transactional
@Slf4j
public class GithubApiServiceImpl implements GithubApiService{

    private final GithubApiConfig githubApiConfig;

    private final WebClient webClient;

    @Autowired
    public GithubApiServiceImpl(GithubApiConfig githubApiConfig) {
        this.githubApiConfig = githubApiConfig;
        this.webClient = WebClient.create(githubApiConfig.getUrl());
    }

    @Override
    public GithubUserDetailsDto getGithubUserDetailsDtoByLogin(String login) {
        log.info("Retrieve GithubUserDetails for login {}", login);

        return webClient.get()
                .uri(githubApiConfig.getUri(), login)
                .accept(MediaType.APPLICATION_JSON)
                .retrieve()
                .toEntity(GithubUserDetailsDto.class)
                .block()
                .getBody();
    }




}
