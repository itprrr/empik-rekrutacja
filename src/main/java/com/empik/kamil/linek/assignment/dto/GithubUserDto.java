package com.empik.kamil.linek.assignment.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;

@Getter
@Setter
public abstract class GithubUserDto {
    protected Long id;
    protected String login;
    protected String name;
    protected String type;
    @JsonProperty("avatar_url")
    protected String avatarUrl;
    @JsonProperty("created_at")
    protected LocalDateTime createdAt;
}
