package com.empik.kamil.linek.assignment.dto;

import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;

@Getter
@Setter
public class GithubUserCalculationsDto extends GithubUserDto{
    private BigDecimal calculations;
}
