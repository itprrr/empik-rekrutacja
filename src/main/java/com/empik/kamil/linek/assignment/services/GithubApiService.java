package com.empik.kamil.linek.assignment.services;

import com.empik.kamil.linek.assignment.dto.GithubUserDetailsDto;

public interface GithubApiService {
    GithubUserDetailsDto getGithubUserDetailsDtoByLogin(String login);
}
