package com.empik.kamil.linek.assignment.controllers;

import com.empik.kamil.linek.assignment.dto.GithubUserCalculationsDto;
import com.empik.kamil.linek.assignment.services.GithubUserDataService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequestMapping(path = "/users")
public class GithubUserDataController {

    private final GithubUserDataService githubUserDataService;

    @Autowired
    public GithubUserDataController(GithubUserDataService githubUserDataService) {
        this.githubUserDataService = githubUserDataService;
    }

    @GetMapping(path="/{login}", produces = MediaType.APPLICATION_JSON_VALUE)
    public GithubUserCalculationsDto getGithubUserCalculation(@PathVariable String login){

        if(StringUtils.isEmpty(login)){
            throw new IllegalArgumentException("Login value cannot be empty");
        }

        return githubUserDataService.getGithubUserDtoByLogin(login);
    }

}
