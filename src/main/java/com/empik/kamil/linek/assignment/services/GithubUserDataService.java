package com.empik.kamil.linek.assignment.services;

import com.empik.kamil.linek.assignment.dto.GithubUserCalculationsDto;

public interface GithubUserDataService {

    GithubUserCalculationsDto getGithubUserDtoByLogin(String login);
}
