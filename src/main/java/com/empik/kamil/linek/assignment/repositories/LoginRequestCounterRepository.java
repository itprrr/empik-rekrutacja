package com.empik.kamil.linek.assignment.repositories;

import com.empik.kamil.linek.assignment.entities.LoginRequestCounter;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

public interface LoginRequestCounterRepository extends JpaRepository<LoginRequestCounter, Long> {

    boolean existsByLogin(String login);

    @Modifying
    @Query("UPDATE LoginRequestCounter SET requestCount = requestCount + 1 WHERE login = ?1")
    void incrementCounterForLogin(String login);

}
