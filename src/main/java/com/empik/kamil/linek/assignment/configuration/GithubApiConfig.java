package com.empik.kamil.linek.assignment.configuration;

import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

@Getter
@Setter
@Configuration
@PropertySource("classpath:api.properties")
public class GithubApiConfig {

    @Value("${github.api.url}")
    private String url;

    @Value("${github.api.uri.user.details}")
    private String uri;
}
