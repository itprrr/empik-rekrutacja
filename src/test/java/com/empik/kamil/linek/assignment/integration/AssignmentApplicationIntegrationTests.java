package com.empik.kamil.linek.assignment.integration;

import com.empik.kamil.linek.assignment.AssignmentApplication;
import com.empik.kamil.linek.assignment.dto.GithubUserCalculationsDto;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;

import java.math.BigDecimal;

@SpringBootTest(classes = AssignmentApplication.class,
        webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class AssignmentApplicationIntegrationTests {

    @LocalServerPort
    private int port;

    @Autowired
    private TestRestTemplate restTemplate;

    @Test
    public void testAssignmentApplication(){
        final String login = "octocat";
        final GithubUserCalculationsDto userCalculations = this.restTemplate
                .getForObject("http://localhost:" + port + "/users/" + login, GithubUserCalculationsDto.class);
        Assertions.assertEquals(userCalculations.getLogin(), login);
        Assertions.assertTrue(BigDecimal.ZERO.compareTo(userCalculations.getCalculations()) < 0 );
    }


}
