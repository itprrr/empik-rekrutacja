package com.empik.kamil.linek.assignment.utils;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.math.BigDecimal;

@SpringBootTest
class UtilsTest {

    @Test
    void testCalculate() {
        BigDecimal result = Utils.calculate(1L, 0);
        Assertions.assertTrue(result.compareTo(BigDecimal.ZERO) > 0);
    }

    @Test
    void testCalculateExpectedException() {
        IllegalArgumentException exception = Assertions.assertThrows(IllegalArgumentException.class,
                () -> Utils.calculate(0L, 0));
        Assertions.assertTrue(exception.getMessage().contains("zero"));
    }
}

