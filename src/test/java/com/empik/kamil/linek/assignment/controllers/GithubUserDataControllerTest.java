package com.empik.kamil.linek.assignment.controllers;

import com.empik.kamil.linek.assignment.dto.GithubUserCalculationsDto;
import com.empik.kamil.linek.assignment.services.GithubUserDataService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@SpringBootTest
@AutoConfigureMockMvc
class GithubUserDataControllerTest {

    @MockBean
    private GithubUserDataService githubUserDataService;

    @Autowired
    private MockMvc mockMvc;

    @Test
    void testGetGithubUserCalculation() throws Exception {
        final String login = "octocat";
        GithubUserCalculationsDto userCalculationsDto = new GithubUserCalculationsDto();
        userCalculationsDto.setLogin(login);

        when(githubUserDataService.getGithubUserDtoByLogin(login)).thenReturn(userCalculationsDto);

        this.mockMvc.perform(get("/users/{login}",login))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON));
    }
}

